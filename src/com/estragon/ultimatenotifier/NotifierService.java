package com.estragon.ultimatenotifier;

import android.accessibilityservice.AccessibilityService;
import android.content.Intent;
import android.view.accessibility.AccessibilityEvent;

public class NotifierService extends AccessibilityService {

	@Override
	public void onAccessibilityEvent(AccessibilityEvent evt) {
		if (evt.getPackageName().equals("com.estragon.ultimatenotifier")) {
			
		}
		else if (evt.getEventType() == AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED) {
			String texte = "";
			for (CharSequence seq : evt.getText()) {
				texte += seq;
			}
			Intent i = new Intent(this, NotifSmartService.class);
			i.putExtra("titre",evt.getBeforeText());
			i.putExtra("texte",texte);
			i.setAction(NotifSmartService.ACTION_NEW_NOTIFICATION);
			startService(i);
		}
	}

	@Override
	public void onInterrupt() { }
}