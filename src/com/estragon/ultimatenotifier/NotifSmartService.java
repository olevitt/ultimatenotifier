package com.estragon.ultimatenotifier;

import java.util.Random;

import android.content.ContentValues;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.sonyericsson.extras.liveware.aef.notification.Notification;
import com.sonyericsson.extras.liveware.extension.util.ExtensionService;
import com.sonyericsson.extras.liveware.extension.util.ExtensionUtils;
import com.sonyericsson.extras.liveware.extension.util.notification.NotificationUtil;
import com.sonyericsson.extras.liveware.extension.util.registration.RegistrationInformation;

public class NotifSmartService extends ExtensionService {
	
	public static String ACTION_NEW_NOTIFICATION = "con.estragon.ultimatenotifier.newnotification";

	public NotifSmartService() {
		super(NotifSmartInformations.EXTENSION_KEY);
	}

	@Override
	protected RegistrationInformation getRegistrationInformation() {
		return new NotifSmartInformations(this);
	}

	@Override
	protected boolean keepRunningWhenConnected() {
		return false;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int retVal = super.onStartCommand(intent, flags, startId);
		if (intent != null) {
			if (ACTION_NEW_NOTIFICATION.equals(intent.getAction())) {
				addData(intent.getStringExtra("titre"),intent.getStringExtra("texte"));
			}
		}
		return retVal;
	}

	private void addData(String name, String message) {
		long time = System.currentTimeMillis();
		long sourceId = NotificationUtil
				.getSourceId(this, NotifSmartInformations.EXTENSION_SPECIFIC_ID);
		if (sourceId == NotificationUtil.INVALID_ID) {
			Log.e("UltimateNotifier", "Failed to insert data");
			return;
		}
		String profileImage = ExtensionUtils.getUriString(this,
				R.drawable.ic_launcher);
		ContentValues eventValues = new ContentValues();
		eventValues.put(Notification.EventColumns.EVENT_READ_STATUS, false);
		eventValues.put(Notification.EventColumns.DISPLAY_NAME, name);
		eventValues.put(Notification.EventColumns.MESSAGE, message);
		eventValues.put(Notification.EventColumns.PERSONAL, 0);
		eventValues.put(Notification.EventColumns.PROFILE_IMAGE_URI, profileImage);
		eventValues.put(Notification.EventColumns.PUBLISHED_TIME, time);
		eventValues.put(Notification.EventColumns.SOURCE_ID, sourceId);
		try {
			getContentResolver().insert(Notification.Event.URI, eventValues);
		} catch (IllegalArgumentException e) {
			Log.e("UltimateNotifier", "Failed to insert event", e);
		} catch (SecurityException e) {
			Log.e("UltimateNotifier", "Failed to insert event, is Live Ware Manager installed?", e);
		}
	}

}
