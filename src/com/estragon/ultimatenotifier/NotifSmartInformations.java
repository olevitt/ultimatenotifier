package com.estragon.ultimatenotifier;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;

import com.sonyericsson.extras.liveware.aef.notification.Notification;
import com.sonyericsson.extras.liveware.aef.registration.Registration;
import com.sonyericsson.extras.liveware.extension.util.ExtensionUtils;
import com.sonyericsson.extras.liveware.extension.util.registration.RegistrationInformation;

public class NotifSmartInformations extends RegistrationInformation {

	public static final String EXTENSION_KEY = "com.estragon.ultimatenotifier.key";
	public static final String EXTENSION_SPECIFIC_ID = "com.estragon.ultimatenotifier.specificID";
	final Context mContext;

	/**
	 * Create control registration object
	 *
	 * @param context The context
	 */
	protected NotifSmartInformations(Context context) {
		if (context == null) {
			throw new IllegalArgumentException("context == null");
		}
		mContext = context;
	}

	@Override
	public int getRequiredControlApiVersion() {
		return 1;
	}

	@Override
	public int getRequiredSensorApiVersion() {
		return 0;
	}

	@Override
	public int getRequiredNotificationApiVersion() {
		return 1;
	}

	@Override
	public int getRequiredWidgetApiVersion() {
		return 0;
	}



	/**
	 * Get the extension registration information.
	 *
	 * @return The registration configuration.
	 */
	@Override
	public ContentValues getExtensionRegistrationConfiguration() {
		String iconHostApp = ExtensionUtils.getUriString(mContext, R.drawable.ic_launcher);
		String iconExtension = ExtensionUtils
				.getUriString(mContext, R.drawable.ic_launcher);

		ContentValues values = new ContentValues();

		values.put(Registration.ExtensionColumns.CONFIGURATION_TEXT,"Configuration");
		values.put(Registration.ExtensionColumns.NAME, "UltimateNotifier");
		values.put(Registration.ExtensionColumns.EXTENSION_KEY,EXTENSION_KEY);
		values.put(Registration.ExtensionColumns.HOST_APP_ICON_URI, iconHostApp);
		values.put(Registration.ExtensionColumns.EXTENSION_ICON_URI, iconExtension);
		values.put(Registration.ExtensionColumns.NOTIFICATION_API_VERSION,
				getRequiredNotificationApiVersion());
		values.put(Registration.ExtensionColumns.PACKAGE_NAME, mContext.getPackageName());
		return values;
	}

	@Override
	public boolean isDisplaySizeSupported(int width, int height) {
		return true;
	}

	@Override
	public ContentValues[] getSourceRegistrationConfigurations() {
		List<ContentValues> bulkValues = new ArrayList<ContentValues>();
		bulkValues.add(getSourceRegistrationConfiguration(EXTENSION_SPECIFIC_ID));
		return bulkValues.toArray(new ContentValues[bulkValues.size()]);
	}

	public ContentValues getSourceRegistrationConfiguration(String extensionSpecificId) {
		ContentValues sourceValues = null;
		String iconSource1 = ExtensionUtils.getUriString(mContext,
				R.drawable.ic_launcher);
		String iconSource2 = ExtensionUtils.getUriString(mContext,
				R.drawable.ic_launcher);
		String actionEvent1 = "action1";
		sourceValues = new ContentValues();
		//sourceValues.put(Notification.SourceColumns.ACTION_1, actionEvent1);
		sourceValues.put(Notification.SourceColumns.ENABLED, true);
		sourceValues.put(Notification.SourceColumns.ICON_URI_1, iconSource1);
		sourceValues.put(Notification.SourceColumns.ICON_URI_2, iconSource2);
		sourceValues.put(Notification.SourceColumns.UPDATE_TIME,
				System.currentTimeMillis());
		// Source specific values
		sourceValues.put(Notification.SourceColumns.NAME,"UltimateNotifier");
		sourceValues.put(Notification.SourceColumns.EXTENSION_SPECIFIC_ID,
				extensionSpecificId);
		return sourceValues;
	}




}
