package com.estragon.ultimatenotifier;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class NotifSmartReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(final Context context, final Intent intent) {
		Log.d("Notif", "onReceive: " + intent.getAction());
		intent.setClass(context, NotifSmartService.class);
		context.startService(intent);
	}
	
	
}
